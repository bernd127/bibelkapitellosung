//// Kennzeichnung für automatisch generierten Kotlin-Code, der generell für die Darstellung der
//// Benutzeroberfläche aus der Datei activity_main.xml sorgt (wie bei hallowelt).

/// Kommentare für zusätzlichen allgemeinen technischen Code

// BiKaLo-spezifische Kommentare

package de.bernd.bikalo //// Nach dem Schlüsselwort package folgt der eindeutige Name des Pakets dieses
                        //// Projekts, wie er bei Erstellung des Projekts erzeugt wurde (anstelle von
                        //// »package com.example.hallowelt«)
import androidx.appcompat.app.AppCompatActivity ////
import android.os.Bundle ////

import de.bernd.bikalo.databinding.ActivityMainBinding /// Fürs View Binding: Die automatisch generierte
/// Klasse ActivityMainBinding wird importiert (siehe Seite 246, 247)

import kotlin.random.Random // erforderlich für Random(System.currentTimeMillis()) bzw. Random(System.nanoTime())

class MainActivity : AppCompatActivity() { //// Die Klasse AppCompatActivity dient als
//// Basisklasse (Oberklasse, allgemeine Klasse) für die Klasse MainActivity. Dabei handelt
//// es sich um die Klasse für die einzige Activity der App. Mithilfe der Klasse AppCompatActivity
//// wird eine Abwärtskompatibilität ermöglicht, mit der sich neuere Android-Eigenschaften auch
//// auf älteren Android-Geräten nutzen lassen.
private lateinit var B: ActivityMainBinding /// fürs View Binding
    override fun onCreate(savedInstanceState: Bundle?) { //// Die Methode onCreate() wird beim Start
                                                         //// der Activity aufgerufen...
    //// Mithilfe des nullbaren Objekts »savedInstanceState« der Klasse »Bundle« können Informationen
    //// zwischen Activitys transportiert werden.
        super.onCreate(savedInstanceState) //// ...sie ruft mithilfe des Schlüsselworts »super« die
                                           //// gleichnamige Methode der Basisklasse auf.
        B = ActivityMainBinding.inflate(layoutInflater) /// fürs View Binding
        setContentView(B.root) /// Die Methode setContentView() füllt die Activity mit einem sichtbaren
        /// Inhalt. Das geschieht über das Hauptelement (oder die Wurzel, englisch: root) des Layouts aus
        /// der verbundenen Layout-Datei »activity_main.xml«
    //// setContentView(R.layout.activity_main) //// Ist somit obsolet!

        B.button.setOnClickListener { /// Mithilfe des Binding-Objekts wird die Verbindung zum Button
            /// »button« sowie ...*

            val books = arrayOf<String>(
                "0", // DummyFürIndex0
                "1. Mose","2. Mose","3. Mose","4. Mose","5. Mose",               // Das Gesetz
                "Josua","Richter","Ruth","1. Samuel","2. Samuel",                // Geschichtsbücher
                "1. Könige","2. Könige","1. Chronik","2. Chronik",               // Geschichtsbücher
                "Esra","Nehemia","Esther",                                       // Geschichtsbücher

                "Hiob","Psalmen","Sprüche","Prediger","Hohelied",                // Weisheitsbücher
                "Jesaja","Jeremia","Klagelieder","Hesekiel","Daniel",            // Große Propheten
                "Hosea","Joel","Amos","Obadia","Jona","Micha","Nahum",           // Kleine Propheten
                "Habakuk","Zefanja","Haggai","Sacharja","Maleachi",              // Kleine Propheten

                "Matthäus","Markus","Lukas","Johannes",                          // Evangelien
                "Apostelgeschichte",                                             // Apostelgeschichte
                "Römer","1. Korinther","2. Korinther","Galater","Epheser",       // Paulus-Briefe
                "Philipper","Kolosser","1. Thessalonicher","2. Thessalonicher",  // Paulus-Briefe
                "1. Timotheus","2. Timotheus","Titus","Philemon",                // Paulus-Briefe
                "Hebräer","Jakobus","1. Petrus","2. Petrus",                     // Briefe
                "1. Johannes","2. Johannes","3. Johannes","Judas",               // Briefe
                "Offenbarung"                                                    // Offenbarung
            )
            val chapters = arrayOf<Int>(
                0, // DummyFürIndex0
                50,40,27,36,34,  // Gesetz
                24,21,4,31,24,   // Geschichtsbücher
                22,25,29,36,     // Geschichtsbücher
                10,13,10,        // Geschichtsbücher

                42,150,31,12,8,  // Weisheitsbücher
                66,52,5,48,12,   // Große Propheten
                14,4,9,1,4,7,3,  // Kleine Propheten
                3,3,2,14,3,      // Kleine Propheten

                28,16,24,21,     // Evangelien
                28,              // Apostelgeschichte
                16,16,13,6,6,    // Paulus-Briefe
                4,4,5,3,         // Paulus-Briefe
                6,4,3,1,         // Paulus-Briefe
                13,5,5,3,        // Briefe
                5,1,1,1,         // Briefe
                22               // Offenbarung
            )

            // val randNumber = (1 .. 66).random() // ermittelt Zufallszahl 1...66  liefert immer gleiche Folge :-(
            // val randNumber = Random.nextInt(1, 67) // ermittelt Zufallszahl 1...66  liefert ebenfalls immer gleiche Folge :-(
            val rand = Random(System.currentTimeMillis()) // Random(..) ist zusätzlich als "Saat-Generator-Zwischenschritt" erforderlich ...
            val randNumber = (1 .. 66).random(rand) // ... weil "Kurzform" random(System.currentTimeMillis()) Fehlermeldung ergibt

            val selectBook = books[randNumber]   // holt ermittelte Buchbezeichnung aus Array;
                                                 // selectBook ist implizite String-Variable
            val bookChapters = chapters[randNumber] // hole Gesamtkapitelanzahl für ermitteltes Buch;
                                                    // bookChapters ist implizite Int-Variable

            val rand2 = Random(System.currentTimeMillis()) // weitere Variable aus "ABSOLUTEN Millisekunden" für Kapitel-Losung generiert
                                                           // zur Vermeidung von öfterem "Pseudozufallsgenerator-WIEDERHOLUNGseffekt"
            val selectChapter = (1 .. bookChapters).random(rand2) // ergibt 1...bookChapters

            if(randNumber == 19) { // falls gewähltes Buch  Psalmen (Buch 19)  ist, gebe String »den Psalm« anstelle »das Kapitel« aus
                B.tvAusgabe.text = "Lese im Buch\n$selectBook\nden Psalm $selectChapter" /// *... zu der TextView tvAusgabe erstellt.
                /// Der Eigenschaft text der TextView wird (jeweils) eine neue Zeichenkette zugewiesen.
            }
            else { // gebe standardmäßig den String »das Kapitel« aus
                B.tvAusgabe.text = "Lese im Buch\n$selectBook\ndas Kapitel $selectChapter" /// *... zu der TextView tvAusgabe erstellt.
                /// Der Eigenschaft text der TextView wird (jeweils) eine neue Zeichenkette zugewiesen.
            }
        }
    } ////
} ////
